const data = require('./data.json');

const getRequiredFuel = (mass) => {
    const fuelAmount = Math.floor(mass / 3) - 2;

    if (fuelAmount <= 0) {
        return 0;
    }

    return getRequiredFuel(fuelAmount) + fuelAmount;
}

console.assert(getRequiredFuel(12) == 2);
console.assert(getRequiredFuel(14) == 2);
console.assert(getRequiredFuel(1969) == 966);
console.assert(getRequiredFuel(100756) == 50346);

const fuel = data
    .map(getRequiredFuel)
    .reduce((p, c) => p + c, 0);

console.log('Total amount of fuel: ', fuel);
